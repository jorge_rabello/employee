package br.com.jorgerabellodev.employee.api.errors.details;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@RequiredArgsConstructor
public class ExceptionDetails {

  private final String message;

}
