package br.com.jorgerabellodev.employee.api.repository;

import br.com.jorgerabellodev.employee.domain.entities.Employee;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

  Optional<Employee> findByCpf(String cpf);
}
