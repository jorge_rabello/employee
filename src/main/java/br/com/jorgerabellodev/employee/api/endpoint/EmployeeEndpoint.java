package br.com.jorgerabellodev.employee.api.endpoint;


import br.com.jorgerabellodev.employee.api.service.EmployeeService;
import br.com.jorgerabellodev.employee.domain.dto.EmployeeRequest;
import br.com.jorgerabellodev.employee.domain.dto.EmployeeResponse;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/employee")
public class EmployeeEndpoint {

  private final EmployeeService service;

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> findAll() {
    List<EmployeeResponse> allEmployees = service.findAll();
    return new ResponseEntity<>(allEmployees, HttpStatus.OK);
  }

  @GetMapping(value = "/findById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> findById(@PathVariable("id") Long id) {
    EmployeeResponse employee = service.findById(id);
    return new ResponseEntity<>(employee, HttpStatus.OK);
  }

  @GetMapping(value = "/findByCpf/{cpf}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> findByCPF(@PathVariable("cpf") String cpf) {
    EmployeeResponse employee = service.findByCpf(cpf);
    return new ResponseEntity<>(employee, HttpStatus.OK);
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> newEmployee(@RequestBody EmployeeRequest EmployeeRequest) {
    EmployeeResponse savedEmployee = service.save(EmployeeRequest);
    return new ResponseEntity<>(savedEmployee, HttpStatus.CREATED);
  }

  @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> updateEmployee(@RequestBody EmployeeRequest employeeRequest, @PathVariable("id") Long id) {
    EmployeeResponse updatedEmployee = service.update(employeeRequest, id);
    return new ResponseEntity<>(updatedEmployee, HttpStatus.OK);
  }

  @DeleteMapping("{id}")
  public ResponseEntity<?> deleteEmployee(@PathVariable("id") Long id) {
    service.delete(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
