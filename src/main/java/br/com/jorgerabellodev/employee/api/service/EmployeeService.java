package br.com.jorgerabellodev.employee.api.service;

import br.com.jorgerabellodev.employee.api.errors.exceptions.ResourceNotFoundException;
import br.com.jorgerabellodev.employee.api.repository.EmployeeRepository;
import br.com.jorgerabellodev.employee.domain.dto.EmployeeRequest;
import br.com.jorgerabellodev.employee.domain.dto.EmployeeResponse;
import br.com.jorgerabellodev.employee.domain.entities.Employee;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmployeeService {

  private final EmployeeRepository repository;

  public List<EmployeeResponse> findAll() {
    List<Employee> allEmployees = repository.findAll();
    List<EmployeeResponse> allEmployeesResponse = new ArrayList<>();
    allEmployees.forEach(employee -> {
      EmployeeResponse employeeResponse = EmployeeResponse
          .builder()
          .id(employee.getId())
          .name(employee.getName())
          .surname(employee.getSurname())
          .cpf(employee.getCpf())
          .build();
      allEmployeesResponse.add(employeeResponse);
    });
    return allEmployeesResponse;
  }

  public EmployeeResponse findById(Long id) {
    Optional<Employee> employee = repository.findById(id);
    if (!employee.isPresent()) {
      throw new ResourceNotFoundException("Employee not found for id: " + id);
    }
    return EmployeeResponse
        .builder()
        .id(employee.get().getId())
        .name(employee.get().getName())
        .surname(employee.get().getSurname())
        .cpf(employee.get().getCpf())
        .build();
  }

  public EmployeeResponse findByCpf(String cpf) {
    Optional<Employee> Employee = repository.findByCpf(cpf);
    if (!Employee.isPresent()) {
      throw new ResourceNotFoundException("Employee not found for cpf: " + cpf);
    }
    return EmployeeResponse
        .builder()
        .id(Employee.get().getId())
        .name(Employee.get().getName())
        .surname(Employee.get().getSurname())
        .cpf(Employee.get().getCpf())
        .build();
  }

  public EmployeeResponse save(EmployeeRequest EmployeeRequest) {
    Employee employee = Employee
        .builder()
        .name(EmployeeRequest.getName())
        .surname(EmployeeRequest.getSurname())
        .cpf(EmployeeRequest.getCpf())
        .build();
    Employee savedEmployee = repository.save(employee);
    return EmployeeResponse
        .builder()
        .id(savedEmployee.getId())
        .name(savedEmployee.getName())
        .surname(savedEmployee.getSurname())
        .cpf(savedEmployee.getCpf())
        .build();
  }

  public EmployeeResponse update(EmployeeRequest employeeRequest, Long id) {
    Optional<Employee> optionalEmployee = repository.findById(id);
    if (!optionalEmployee.isPresent()) {
      throw new ResourceNotFoundException("Employee not found by id: " + id);
    }
    Employee employee = Employee
        .builder()
        .id(id)
        .name(employeeRequest.getName())
        .surname(employeeRequest.getSurname())
        .cpf(employeeRequest.getCpf())
        .build();
    Employee updatedEmployee = repository.save(employee);
    return EmployeeResponse
        .builder()
        .id(updatedEmployee.getId())
        .name(updatedEmployee.getName())
        .surname(updatedEmployee.getSurname())
        .cpf(updatedEmployee.getCpf())
        .build();
  }

  public void delete(Long id) {
    Optional<Employee> optionalEmployee = repository.findById(id);
    if (!optionalEmployee.isPresent()) {
      throw new ResourceNotFoundException("Employee not found by id: "+ id);
    }
    repository.deleteById(id);
  }
}
