package br.com.jorgerabellodev.employee.domain.dto;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@RequiredArgsConstructor
public class EmployeeResponse {

  private final Long id;
  private final String name;
  private final String surname;
  private final String cpf;

}
